# work-single-view-script

This is an example of an script that process musical works uploaded through an csv file.

The technologic stack consists in :
- Script: developed in Python 3.7
- Database: a Postgre 12.4 database


##### Table of Contents
1. [Getting Started](#1-getting-started)
2. [Working in the project](#2-working-in-the-project)


## 1. Getting Started
-----------------------

### 1.1. __Prerequisites__

You need to have installed:
- docker
- docker-compose

#### __How to install Docker__

Here are some links to install Docker in [Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/), [Mac](https://docs.docker.com/docker-for-mac/install/) and [Windows](https://docs.docker.com/docker-for-windows/install/).

#### __How to install docker-compose__

In this [link](https://docs.docker.com/compose/install/) there is information about how to install docker-compose in the Ubuntu, Mac and Windows OS.


## 2. Working in the project
------------------------------

This project is build in a dockerized environment and it has some peculiarities. Everything the developer needs is inside the backend container. 


### 2.1. __Run the script__

To run the script, first you have to build the image. As this project is also configured with docker-compose you just have to run:

`docker-compose up`

The first time it will build the image for the environment where the script is going to be executed.

Once is finished, you can access the container by running:

`docker-compose run script-env sh`

script-env is the name of the service defined in the docker-compose file.

Once you are inside the container you can run the script:

`python single_view.py`

**NOTE:** The files that you want to process have to be put it into the `csv_files` directory.


### 2.2. __Access Postgre Database__

This project starts a Postgres DB when you open the project with the extension. This database is run inside a container.

If you want to access the database you can run:

`docker-compose run database bash`

Once you are in, you can connect to the database:

`PGPASSWORD=supercomplicatedpassword psql -h database -d wsv_db -U postgres`

And make some queries like:

`SELECT * FROM works;`


## 2.3. __Run the tests__

To run the test, you have to pass the some enviromental variables such as the CSV_PATH, where the tests will create a csv file, and the DB_URI:

`CSV_PATH=test_csv_files DB_URI=postgresql://postgres:supercomplicatedpassword@database-test:5432/wsv_db python tests.py` 


