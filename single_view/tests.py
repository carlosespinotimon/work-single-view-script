import unittest
import testing.postgresql
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
import os

from single_view import db


from single_view import main as single_view


class MyTestCase(unittest.TestCase):

    def _sort(self, list_):
        return sorted(list_.split("|"))


    def setUp(self):
        engine = create_engine(os.environ.get(
            "DB_URI", 
            'postgresql://postgres:supercomplicatedpassword@database-test:5432/wsv_db_test'))
   
        Session = sessionmaker(bind=engine)
        self.conn = engine.connect()
        self.session = Session(bind=self.conn)
    
        

    def tearDown(self):
        self.conn.execute("DELETE FROM works;")
        os.remove('test_csv_files/script.csv')
        self.conn.close()
        self.session.close()

    def test_insert_3_different_titles_for_same_work(self):
        with open('test_csv_files/script.csv', "w") as wf:
            wf.write("title,contributors,iswc,source,id\r\n")
            wf.write(
                "Title of the song,contributor 1|contributor 2,T123456789,entity,1\r\n")
            wf.write(
                "Title of the song 2,contributor 3|contributor 2,T123456789,another entity,2\r\n")
            wf.write(
                "Title of the song 3,contributor 3|contributor 2,T123456789,another entity,2")
        single_view()

        result = self.conn.execute(
            "SELECT * FROM works WHERE iswc = 'T123456789';")
        self.assertEqual(result.rowcount, 1)
        stored_work = {k: v for k, v in result.fetchone().items()}
        self.assertEqual(
            self._sort(stored_work['title']),
            self._sort(
                'Title of the song|Title of the song 2|Title of the song 3')
        )

    def test_insert_same_work_3_times_with_3_contributors(self):
        with open('test_csv_files/script.csv', "w") as wf:
            wf.write("title,contributors,iswc,source,id\r\n")
            wf.write(
                "Title of the song,contributor 1|contributor 2,T123456789,entity,1\r\n")
            wf.write(
                "Title of the song 2,contributor 3|contributor 2,T123456789,another entity,2\r\n")
            wf.write(
                "Title of the song 3,contributor 3|contributor 2,T123456789,another entity,2")
        single_view()

        result = self.conn.execute(
            "SELECT * FROM works WHERE iswc = 'T123456789';")
        self.assertEqual(result.rowcount, 1)
        stored_work = {k: v for k, v in result.fetchone().items()}
        self.assertEqual(
            self._sort(stored_work['contributors']),
            self._sort(
                'contributor 1|contributor 2|contributor 3')
        )

    def test_insert_same_work_3_times_with_2_tuples_provider_and_id(self):
        with open('test_csv_files/script.csv', "w") as wf:
            wf.write("title,contributors,iswc,source,id\r\n")
            wf.write(
                "Title of the song,contributor 1|contributor 2,T123456789,entity,1\r\n")
            wf.write(
                "Title of the song 2,contributor 3|contributor 2,T123456789,another entity,2\r\n")
            wf.write(
                "Title of the song 3,contributor 3|contributor 2,T123456789,another entity,2")
        single_view()

        result = self.conn.execute(
            "SELECT * FROM works WHERE iswc = 'T123456789';")
        self.assertEqual(result.rowcount, 1)
        stored_work = {k: v for k, v in result.fetchone().items()}
        self.assertEqual(
            self._sort(stored_work['source']),
            self._sort(
                'entity--1|another entity--2')
        )



    def test_insert_same_work_3_times(self):
        with open('test_csv_files/script.csv', "w") as wf:
            wf.write("title,contributors,iswc,source,id\r\n")
            wf.write(
                "Title of the song,contributor 1|contributor 2,T123456789,entity,1\r\n")
            wf.write(
                "Title of the song 2,contributor 3|contributor 2,T123456789,another entity,2\r\n")
            wf.write(
                "Title of the song 3,contributor 3|contributor 2,T123456789,another entity,2")
        single_view()

        result = self.conn.execute("SELECT * FROM works WHERE iswc = 'T123456789';")
        self.assertEqual(result.rowcount, 1)
        stored_work = {k:v for k,v in result.fetchone().items()}
        self.assertEqual(
            self._sort(stored_work['title']),
            self._sort(
                'Title of the song|Title of the song 2|Title of the song 3')
        )
        self.assertEqual(
            self._sort(stored_work['contributors']),
            self._sort(
                'contributor 1|contributor 2|contributor 3')
        )
        self.assertEqual(
            self._sort(stored_work['source']),
            self._sort(
                'entity--1|another entity--2')
        )


    def test_insert_two_works(self):
        with open('test_csv_files/script.csv', "w") as wf:
            wf.write("title,contributors,iswc,source,id\r\n")
            wf.write(
                "Title of the song,contributor 1|contributor 2,T123456789,entity,1\r\n")
            wf.write(
                "Title of the song 2,contributor 3|contributor 2,T123456790,another entity,2\r\n")
            
        single_view()

        result = self.conn.execute("SELECT * FROM works;")
        self.assertEqual(result.rowcount, 2)


    def test_insert_three_works_one_without_iswc(self):
        with open('test_csv_files/script.csv', "w") as wf:
            wf.write("title,contributors,iswc,source,id\r\n")
            wf.write(
                "Title of the song,contributor 1|contributor 2,T123456789,entity,1\r\n")
            wf.write(
                "Title of the song 2,contributor 3|contributor 2,T123456790,another entity,2\r\n")
            wf.write(
                "Title of the song 2,contributor 3|contributor 2,,another entity,2\r\n")
            
        single_view()

        result = self.conn.execute("SELECT * FROM works;")
        self.assertEqual(result.rowcount, 2)

    def test_insert_three_works_one_without_empty_string_as_iswc(self):
        with open('test_csv_files/script.csv', "w") as wf:
            wf.write("title,contributors,iswc,source,id\r\n")
            wf.write(
                "Title of the song,contributor 1|contributor 2,T123456789,entity,1\r\n")
            wf.write(
                "Title of the song 2,contributor 3|contributor 2,T123456790,another entity,2\r\n")
            wf.write(
                "Title of the song 2,contributor 3|contributor 2, ,another entity,2\r\n")
            
        single_view()

        result = self.conn.execute("SELECT * FROM works;")
        self.assertEqual(result.rowcount, 2)
        

if __name__ == '__main__':
    unittest.main()

