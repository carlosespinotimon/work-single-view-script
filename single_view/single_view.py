import collections
import glob
import os

from sqlalchemy import Column, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker

HEADER_FORMAT = ['title', 'contributors', 'iswc', 'source', 'id']

engine = create_engine(
     os.environ.get(
         "DB_URI", 
         'postgresql://postgres:supercomplicatedpassword@database/wsv_db')
    )
db = scoped_session(sessionmaker(bind=engine))
Base = declarative_base()

Work = collections.namedtuple('Work', 'title contributors iswc source id')


class InvalidCSVformat(Exception):
    pass

class StoredWork(Base):
    __tablename__ = 'works'
    iswc = Column(String(36), primary_key=True)
    title = Column(String(128))
    contributors = Column(String(128))
    source = Column(String(128))

    def get_attribute_by_field_name(self, field):
        return getattr(self, str(field))


class WorkCSVLoader():
    def __init__(self):
        self.readed = []
        self.works_loaded = {}
        self.read_files(os.environ.get('CSV_PATH', "csv_files"))
        self.aggregate_content_of_same_works()
        self.remove_duplicates()

    def read_files(self, relative_path):
        for input_file in glob.glob(f'./{relative_path}/*.csv'):
            with open(input_file, "r") as f:
                print(f'Reading  {input_file} file')
                try:
                    header = f.readline().strip().split(",")
                    if not header == HEADER_FORMAT:
                        raise InvalidCSVformat
                    works = [Work._make(work.strip().split(",")) for work in f]
                    # Remove empty iswc
                    self.readed = [w for w in works if w.iswc.strip() != ""]
                except InvalidCSVformat:
                    print(f'File {input_file} not processed, invalid format')

    def aggregate_content_of_same_works(self):
        for work in self.readed:
            if self.works_loaded.get(work.iswc):
                self.works_loaded[work.iswc]['title'].append(work.title)
                self.works_loaded[work.iswc]['contributors'].extend(
                    work.contributors.split("|"))
                self.works_loaded[work.iswc]['source'].append(
                    f'{work.source}--{work.id}')
            else:
                self.works_loaded[work.iswc] = {
                    'title': [work.title],
                    'contributors': work.contributors.split("|"),
                    'source': [f'{work.source}--{work.id}']
                }

    def remove_duplicates(self):
        for iswc, work in self.works_loaded.items():
            self.works_loaded[iswc] = {}
            for k, v in work.items():
                self.works_loaded[iswc][k] = list(set(v))



class WorkProcessor:
    def __init__(self, stored_work, new_work):
        self.stored_work = stored_work
        self.new_work = new_work

    def update_work_and_store_it(self):
        for field in ['title', 'contributors', 'source']:
            self.update_field(field)
        db.add(self.stored_work)

    def update_field(self, field):
        stored_field = list(
            self.stored_work.get_attribute_by_field_name(field).split("|"))
        new_field = self.new_work[field]
        new_field.extend(stored_field)
        # Remove duplicates
        setattr(self.stored_work, field, "|".join(list(set(new_field))))

    @staticmethod
    def get_a_work(iswc):
        return db.query(StoredWork).get(iswc)

    @staticmethod
    def create_work(iswc, new_work):
        work_to_store = StoredWork(
            iswc=iswc,
            title="|".join(new_work['title']),
            contributors="|".join(new_work['contributors']),
            source="|".join(new_work["source"])
        )
        db.add(work_to_store)


def main():
    for iswc, new_work in WorkCSVLoader().works_loaded.items():
        stored_work = WorkProcessor.get_a_work(iswc)
        if stored_work:
            work_processor = WorkProcessor(stored_work, new_work)
            work_processor.update_work_and_store_it()
        else:
            WorkProcessor.create_work(iswc, new_work)
    db.commit()


if __name__ == "__main__":
    main()
